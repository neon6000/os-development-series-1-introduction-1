# OS Development Series #

This is the public source repository for the OS Development Series revision. **This is a work in progress.**

## Directory Structure ##

1. **src** - Project Source Code.
2. **src/Component/** - Project files, organized by component.
3. **src/1_Introduction.sln** - Chapter Solution Files.
4. **src/1_Introduction.mak** - Chapter Make Files.

## Building a specific chapter ##

Go to that chapters SLN or MAK file. For example, Chapter 1 is located at **src/1_Introduction.sln**. Open this file in Visual C++ and build!

## Why this change? ##

The previous versions of the OS Development Series had separate downloads and separate projects per chapter. This means that when a bug or a correction is discovered, all projects needed to be updated and retested. This is becoming increasingly hard and time consuming as the series grows.

In order to simplify this, we decided to try an experiment - to put the entire project in one location only. We still needed a way to separate chapters though, so we did that through the use of makefiles and solution files. The makefiles and solution files filters out the code not part of the chapter. This makes opening chapter specific content easy while also keeping the entire code base in one place.

Please keep in mind however that this is **experimental**. We are always looking for feedback and suggestions! If you have ideas, please let us know!

## Will this be a rewrite? ##

Kind of. A lot of the content will be "remodeled," we want to make sure that all of the content is correct and updated to fix any potential bugs. We also want to give time for our readers to offer suggestions or improvements on either the existing content or new content.

## Will this still be Visual C++ specific? ##

No - we plan to add GCC make file support with the Visual C++ Solution files.

## What do I need in order to build the projects? ##

The original code is being developed in Visual C++, however this release will also be ported to GCC. Assembly language is required in some parts - we use the Netwide Assembler (NASM). We use the Bochs emulator and VirualBox with ImDisk for creating disk images. If you want to boot the system on real hardware, you can use something like Rufus or MagicISO which can be used to copy the image to a USB or CD. We will provide detailed instructions as these tools are introduced in the series.